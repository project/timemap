
Install
--------
1. move to your sites/all/modules directory
2. enable timemap in ?q=/admin/build/modules
3. assign permisions to appropriate roles in ?q=admin/user/access
4. place the 'Time Map Task Entry' block in ?q=admin/build/block

Use
-----
Users can add their own categories, or global categories can be added; all can 
be color coded.  Regardless of category source, users enter tasks as they begin
to work on them and associate the work with a category.  Entering a new task
automatically "closes out" the previous task.  When there is not more "next
task", (ie, user is done doing tasks that should be tracked) he clicks "Quitin' 
Time".  

When tasks and categories are mangaged from the block, the web location will not
change, allowing users to enter their task tracking info without having to "get
back" to where they previously were.


Aims
-----
The project has 2 goals and 1 core principle:

Goal #1:
The data entry into the system should be as absolutely easy as possible.  If it
becomes a chore to enter the data, then people won't do it, and this tool then 
has no advantage over a paper timesheet.

Goal #2:
The reporting be as robust and multi faceted as possible.  For this reason, 
there is a core time mapping report (via the simile timeline library) and will support 
hook_timemap_report(), which will allow 3rd party modules to add their own reports.

Core Principle:
The keeping and maintaining of timesheets is a personal tool to better 
understand what tasks and projects shape the hours of *your* day.  A person's 
understanding of how their own time is spent is much more personaly empowering 
than a facility to track another's time, or a staffs' billable hours, or 
otherwise.  The development of this module will only move in a direction that 
enables more self empowerment to the individual user.  While other appliations 
of this module may certainly develop in parrallel, the facility and 
functionality of the individual user is first priority.



Simile
-------
This module uses the Simile Timeline WebAPI from MIT 
(http://simile.mit.edu/timeline/).  This BSD Licence Library is loaded by 
timemap.module as a WebAPI over the Internet.  If, for any reason, Internet access 
is not avaiable and a local copy is needed for operation, the library can be 
placed in a directory named 'timeline' in the module's directory.  From there, 
it will be auto-detected and loaded locally instead of from the web.

Instructions on obtaining the library can be found at: 
http://simile.mit.edu/wiki/Timeline/Download_The_Source
(If you svn checkout the 1.2 tag, the contents of src/webapp/api are what should
go into the timeline/ directory.) 

Resulting in a dir structure similar to:

timemap/
  |
  |
  -timemap.module
  -timemap.info
  -timemap.install
  ...
  -timeline/
     |
     |
     -bundle.css
     -bundle.js
     -timeline-api.js
     -ext/
     -images/
     -scripts/
     -styles/


