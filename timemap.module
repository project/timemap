<?php

/* TODO Implement the hook_theme registry. Combine all theme registry entries
   into one hook_theme function in each corresponding module file.
function timemap_theme() {
  return array(
    'timemap_admin_settings' => array(
      'file' => 'timemap.module',
      'arguments' => array(
        'form' => NULL,
      ),
    ),
    'timemap_viewable_reports_list_people' => array(
      'file' => 'timemap.module',
      'arguments' => array(
        'list' => array(),
      ),
    ),
  );
} */
/**
 * @file
 * Timemap module - Track how you spend your day with a simple as possible input interface.
 */


/**
 *  Implementation of hook_perm
 */
function timemap_perm() {
  $perms = array(
   'enter tasks',
    'edit own categories',
    'edit global categories',
    'edit timemap settings',
    'view own report',
    'view all reports',
  );

  // If CiviCRM is installed, offer more perms that allow people to view subsets of reports
  // as defined by CiviCRM relationships.
  //
  // TODO: Add a civi version check to the if() statement to make sure we use the correct Civi API calls when
  //       fetching the relationship data.
  if (module_exists('civicrm')) {
    $perms[] = 'view subordinate reports';
  }

  return $perms;
}



/**
 * Implementation of hook_menu
 */
function timemap_menu() {
  $items['timemap'] = array(
    'title' => 'Time Maps',
    'page callback' => 'timemap_dashboard',
    'access arguments' => array('enter tasks'),
  );
  $items['timemap/doing'] = array(
    'title' => 'Enter a task',
    'page callback' => 'timemap_enter_task',
    'access arguments' => array('enter tasks'),
  );
  $items['timemap/map'] = array(
    'title' => 'Personal Time Map',
    'page callback' => 'timemap_map',
    'access arguments' => array('view own report'),
  );
  $items['timemap/report'] = array(
    'title' => 'Time Map Reports',
    'page callback' => 'timemap_report',
    'access arguments' => array('view own report'),
    'type' => MENU_CALLBACK, // technically enable it, but effecively hide it while incomplete.
  );
  $items['admin/settings/timemap'] = array(
    'title' => 'Time Map Settings',
    'description' => 'Change some settings and defaults for the Timemap module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('timemap_admin_settings'),
    'access arguments' => (user_access('edit global categories') || user_access('edit timemap settings')),
  );
  $items['timemap/map/serv'] = array(
    'title' => 'Time Spent Reports Server',
    'page callback' => 'timemap_map_serv',
    'access arguments' => array('view own report'),
    'type' => MENU_CALLBACK,
  );
  $items['timemap/doing/autoc'] = array(
    'title' => 'Task Entry Autocomplete Ajax Callback',
    'page callback' => 'timemap_doing_autoc',
    'access arguments' => array('enter tasks'),
    'type' => MENU_CALLBACK,
  );
  
  return $items;
}



/**
 * Implementation of hook_block
 */
function timemap_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks[0] = array(
        'info' => t('Time Map Task Entry'),
        'status' => true,
        'custom' => false,
        'title' => t('Time Mapping'),
      );
      return $blocks;

    case 'view':
      switch ($delta) {
        case 0:
          $block = array(
            'subject' => t('Time Tracking'),
            'content' => drupal_get_form('timemap_enter_task_form', true),
          );
      }
      return $block;
  }
}

/*******************************************************/

/**
 * form to admin and set the defaults.  the "config" settings
 */
function timemap_admin_settings() {
  global $user;

  $form['#validate'][] = 'timemap_admin_settings_validate';
  $form['#submit'][] = 'timemap_admin_settings_submit';

  $form['gcats'] = array(
    '#type' => 'fieldset',
    '#access' => user_access('edit global categories'),
    '#title' => t('Global Categories'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#weight' => -2,
  );

    // add a new one.
  $form['gcats']['newgcat'] = array(
    '#type' => 'textfield',
    '#description' => t('A new category available to all users.'),
    '#title' => t('New Category'),
  );
  if (module_exists('colorpicker')) {
    $form['gcats']['color_new'] = array(
      '#type' => 'colorpicker',
      '#title' => t('Category Color'),
    );
    $form['gcats']['color_new_v'] = array(
      '#type' => 'colorpicker_textfield',
      '#description' => t('Choose a color to display for this category.'),
      '#default_value' => '#ffffff',
      '#colorpicker' => 'color_new',
    );
  }
  else {
    $form['gcats']['color_new_v'] = array(
      '#type' => 'textfield',
      '#size' => '7',
      '#description' => t('Choose a color to display for this category. Enter it in 6 char hex *with* a preceding #.'),
      '#default_value' => '#ffffff',
    );
  }

    // edit existing ones.
  $result = db_query("select c.cid as cid, c.category as category, c.active as active, cc.hex as hex
                      from {timemap_categories} c left join {timemap_catcolor} cc on c.cid = cc.cid
                      where c.uid = 0");
  while ($row = db_fetch_object($result)) {
    $categories[$row->cid] = array(
      $row->category,
      $row->active,
      $row->hex,
    );
  }

  if (empty($categories)) {
    $categories = array();
  }

  foreach ($categories as $cid => $c) {
    $form['gcats']['cid_enable_'. $cid] = array(
      '#type' => 'radios',
      '#title' => t($c[0]),
      '#options' => array(0 => 'disabled', 1 => 'enabled'),
      '#default_value' => $c[1],
    );
    if (module_exists('colorpicker')) {
      $form['gcats']['color_'. $cid] = array(
        '#type' => 'colorpicker',
        '#title' => t('Color picker'),
      );
      $form['gcats']['color_'. $cid .'_v'] = array(
        '#type' => 'colorpicker_textfield',
        '#description' => t('Choose a color to display for this category.'),
        '#default_value' => $c[2],
        '#colorpicker' => 'color_'. $cid,
      );
    }
    else {
      $form['gcats']['color_'. $cid .'_v'] = array(
        '#type' => 'textfield',
        '#size' => '7',
        '#description' => t('Choose a color to display for this category. Enter it in 6 char hex *with* a preceding #.'),
        '#default_value' => $c[2],
      );
    }
  }

  $form['gsets'] = array(
    '#type' => 'fieldset',
    '#access' => user_access('edit timemap settings'),
    '#title' => t('Global Settings'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#weight' => -1,
  );
  $form['gsets']['timemap_report_default_days_to_show'] = array(
    '#type' => 'textfield',
    '#title' => t('Default number of days to show on reports'),
    '#description' => t('Enter the number of days of history to show when viewing reports that utilize this feature.  (NOTE: This may be N/A for some reports.)'),
    '#default_value' => variable_get('timemap_report_default_days_to_show', 7),
    '#size' => 3,
  );
  if (module_exists('civicrm')) {
    $supervisor_role_opts = array();
    // TODO: This is a CiviCRM 1.8 API call.  Add logic to determine correct CiviCRM version used and API call!!
    foreach(crm_get_relationship_types() as $rel) {
      if (
           $rel->contact_type_a == 'Individual' &&
           $rel->contact_type_b == 'Individual' &&
           $rel->name_a_b != $rel->name_b_a
         ) {
        $supervisor_role_opts[] = $rel->name_a_b;
        $supervisor_role_opts[] = $rel->name_b_a;
      }
    }
    $form['gsets']['timemap_civicrm_subord_rel'] = array(
      '#type' => 'select',
      '#title' => t('Supervisory Relationship'),
      '#description' => t('Select the relationship that defines one CiviCRM user as a Supervisor over others.  This relationship will be used to determain the reports seen when a user has permission to view subordinates\' reports.'),
      '#options' => $supervisor_role_opts,
      '#default_value' => variable_get('timemap_civicrm_subord_rel', null),
    );
  }

  return system_settings_form($form);
}

function timemap_admin_settings_validate($form, &$form_state) {
  if (user_access('edit global categories')) {
    if ($values['color_new_v'] && (! preg_match('/^#[0-9A-F]{6}$/i', $values['color_new_v'])) ) {
      form_set_error('color_new_v', t('Please enter a hex value with a preceding \'#\' sign'));
    }
  }

  if (user_access('edit timemap settings')) {
    if (! is_numeric($values['timemap_report_default_days_to_show'])) {
      form_set_error('timemap_report_default_days_to_show', t('Please enter a number'));
    }
  }
}

function timemap_admin_settings_submit($form, &$form_state) {
   // enter any new global categories
  $newgcat = trim($values['newgcat']);
  if (! empty($newgcat)) {
    db_query("insert into {timemap_categories} (uid, category, active) values (0, '%s', 1)", $newgcat);
    //this makes is mysql only...
    $cid = mysql_insert_id();
    db_query("insert into {timemap_catcolor} (cid, hex) values (%d, '%s')", $cid, $values['color_new_v']);
  }

   //update the status of any categories
  foreach (array_keys($values) as $k) {
    if (strstr($k, 'cid_enable_')) {
      $cid = substr($k, 11);
      db_query("update {timemap_categories} set active = %d where cid = %d", $values[$k], $cid);
      if (db_result(db_query("select cid from {timemap_catcolor} where cid=%d", $cid))) {
        db_query("update {timemap_catcolor} set hex = '%s' where cid = %d", $values['color_'. $cid .'_v'], $cid);
      }
      else {
        db_query("insert into {timemap_catcolor} (cid, hex) values (%d, '%s')", $cid, $values['color_'. $cid .'_v']);
      }
    }
  }

    //update the variables
  if (isset($values['timemap_report_default_days_to_show'])) {
    variable_set('timemap_report_default_days_to_show', (int) $values['timemap_report_default_days_to_show']);
  }
  if (isset($values['timemap_civicrm_subord_rel'])) {
    variable_set('timemap_civicrm_subord_rel', $values['timemap_civicrm_subord_rel']);
  }
}

function theme_timemap_admin_settings($form) {
  //$out .= drupal_render($form['gcats']);
  $out .= '<table border="1"><tr><td>';
  $out .= drupal_render($form['gcats']['newgcat']);
  $out .= '</td><td>';
  $out .= drupal_render($form['gcats']['color_new_v']);
  $out .= '</td><td>';
  $out .= drupal_render($form['gcats']['color_new']);
  $out .= '</td></tr>';

  foreach ($form['gcats'] as $k => $cat) {
    if (strpos($k, 'cid_enable_') !== false) {
      $cid = substr($k, 11);
      $out .= '<tr><td>';
      $out .= drupal_render($form['gcats'][$k]);
      $out .= '</td><td>';
      $out .= drupal_render($form['gcats']['color_'. $cid .'_v']);
      $out .= '</td><td>';
      $out .= drupal_render($form['gcats']['color_'. $cid]);
      $out .= '</td></tr>';
    }
  }
  $out .= '</table>';

  $out .= drupal_render($form);
  return $out;
}


/**
 * a "dashboard" control panel for all of the timemap management facilities.
 */
function timemap_dashboard() {
  drupal_goto('timemap/doing');
}



/**
 *  a callback that loads the form for the task entering interface
 */
function timemap_enter_task() {
  $return  = "<h3>What are you doing right now?</h3>";
  $return .= drupal_get_form('timemap_enter_task_form');
  return $return;
}



/**
 *  The core facility!  this form is the twitter like interface.
 */
function timemap_enter_task_form(&$form_state, $as_block = false) {
  global $user;
  $result = db_query("select cid, category from {timemap_categories} where uid in (0, %d) and active <> 0", $user->uid);
  while ($row = db_fetch_object($result)) {
    $categories[$row->cid] = $row->category;
  }

  $form['newcatfs'] = array(
    '#type' => 'fieldset',
    '#access' => user_access('enter tasks'),
    '#title' => t('New Category'),
    '#collapsible' => true,
    '#collapsed' => true,
    '#weight' => -5,
  );
  $form['newcatfs']['newcat'] = array(
    '#type' => 'textfield',
    '#title' => t('New Category'),
    '#description' => t('Enter a new personal category.'),
    '#required' => false,
    '#size' => 15,
  );
  if (module_exists('colorpicker')) {
    $form['newcatfs']['newcatcolor'] = array(
      '#type' => 'colorpicker',
      '#title' => t('Category Color'),
    );
    $form['newcatfs']['newcatcolor_v'] = array(
      '#type' => 'colorpicker_textfield',
      '#description' => t('Choose a color to display for this category.'),
      '#default_value' => '#ffffff',
      '#colorpicker' => 'newcatcolor',
    );
  }
  else {
    $form['newcatfs']['newcatcolor_v'] = array(
      '#type' => 'textfield',
      '#size' => '7',
      '#description' => t('Choose a color to display for this category. Enter it in 6 char hex *with* a preceding #.'),
      '#default_value' => '#ffffff',
    );
  }
  $form['newcatfs']['newcatsubmit'] = array(
    '#type' => 'submit',
    '#value' => 'Create Category',
  );

  $c_disp = 'radios';
  if ($as_block) {
    $c_disp = 'select';
  }

  if (! empty($categories)) {
    $form['category'] = array(
      '#type' => $c_disp,
      '#title' => t('Category'),
      '#options' => $categories,
      '#description' => 'The task or category that your current doings are a part of.',
    );
  }
  $form['entry'] = array(
    '#type' => 'textfield',
    '#title' => t('Doing Now'),
    '#description' => t('What you are doing right now?'),
    '#required' => false,
    '#size' => 25,
    '#autocomplete_path' => 'timemap/doing/autoc',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Update',
  );

   // is being "done" the last thing we did?  if so, don't show the button again.
  $lastcat = db_result(db_query_range("select cid from {timemap_doings} WHERE uid = %d order by time desc", $user->uid, 0, 1));
  if ($lastcat >= 0) {
    $form['done'] = array(
      '#type' => 'submit',
      '#value' => 'Quitin\' Time!',
    );
  }

  return $form;
}
function timemap_enter_task_form_validate($form, &$form_state) {
/* TODO The 'op' element in the form values is deprecated.
   Each button can have #validate and #submit functions associated with it.
   Thus, there should be one button that submits the form and which invokes
   the normal form_id_validate and form_id_submit handlers. Any additional
   buttons which need to invoke different validate or submit functionality
   should have button-specific functions. */
  if ( (trim($form_state['values']['entry']) == '') && ($form_state['values']['op'] == $form_state['values']['submit']) ) {
    form_set_error('entry');
  }
/* TODO The 'op' element in the form values is deprecated.
   Each button can have #validate and #submit functions associated with it.
   Thus, there should be one button that submits the form and which invokes
   the normal form_id_validate and form_id_submit handlers. Any additional
   buttons which need to invoke different validate or submit functionality
   should have button-specific functions. */
  if ( (trim($form_state['values']['newcat']) == '') && ($form_state['values']['op'] == $form_state['values']['newcatsubmit']) ) {
    form_set_error('newcat');
  }
}
function timemap_enter_task_form_submit($form, &$form_state) {
  global $user;

    // have we entered a new category?
/* TODO The 'op' element in the form values is deprecated.
   Each button can have #validate and #submit functions associated with it.
   Thus, there should be one button that submits the form and which invokes
   the normal form_id_validate and form_id_submit handlers. Any additional
   buttons which need to invoke different validate or submit functionality
   should have button-specific functions. */
  if ($form_state['values']['op'] == $form_state['values']['newcatsubmit']) {
    db_query("insert into {timemap_categories} (uid, category, active) values (%d, '%s', %d)",
              $user->uid, $form_state['values']['newcat'], 1);
    //this makes it mysql only...
    $cid = mysql_insert_id();
    db_query("insert into {timemap_catcolor} (cid, hex) values (%d, '%s')", $cid, $form_state['values']['newcatcolor_v']);
  }

    // have we "clocked out"?
    // if so, enter a negative value for the category.  the text is informational only.
/* TODO The 'op' element in the form values is deprecated.
   Each button can have #validate and #submit functions associated with it.
   Thus, there should be one button that submits the form and which invokes
   the normal form_id_validate and form_id_submit handlers. Any additional
   buttons which need to invoke different validate or submit functionality
   should have button-specific functions. */
  if ($form_state['values']['op'] == $form_state['values']['done']) {
        // end the last 'task' entered
    db_query("update {timemap_doings} set stop=%d where uid=%d order by time desc limit 1", time(), $user->uid);

        // record the 'punch clock' stamp
    db_query("insert into {timemap_doings} (uid, cid, task, time) values (%d, %d, '%s', %d)",
            $user->uid, -1, '***PUNCH CLOCK: OUT***', time());
    drupal_set_message('Until next time...  Thanks champ, you\'re a star!');
  }

    // have we entered a task?
/* TODO The 'op' element in the form values is deprecated.
   Each button can have #validate and #submit functions associated with it.
   Thus, there should be one button that submits the form and which invokes
   the normal form_id_validate and form_id_submit handlers. Any additional
   buttons which need to invoke different validate or submit functionality
   should have button-specific functions. */
  if ($form_state['values']['op'] == $form_state['values']['submit']) {
      // end the last 'task' entered
    db_query("update {timemap_doings} set stop=%d where uid=%d order by time desc limit 1", time(), $user->uid);

      // add the new one
    db_query("insert into {timemap_doings} (uid, cid, task, time) values (%d, %d, '%s', %d)",
              $user->uid, $form_state['values']['category'], $form_state['values']['entry'], time());
    drupal_set_message("roger, Roger: " . $form_state['values']['entry']);
  }
}

function timemap_doing_autoc($string) {
  global $user;
  $result = db_query("select distinct task
                      from {timemap_doings}
                      where uid = %d and task like '%s%%'
                      order by time desc", $user->uid, $string);
  $return = array();
  while ($row = db_fetch_object($result)) {
    $return[$row->task] = $row->task;
  }

  print drupal_to_js($return);
  exit();
}

function timemap_map($uid = null) {
  global $user;
  if (! $uid) {
    $uid = $user->uid;
  }
  else {
    $user = user_load($uid);
  }
  $allowed = timemap_viewable_reports_list();
  if ( ! in_array($uid, array_keys($allowed)) ) {
    drupal_access_denied();
    exit();
  }

  $people_chooser = theme('timemap_viewable_reports_list_people', $allowed);

  /*
   * The Simile Timeline lib from MIT can, per MIT's request, be loaded from the Internet.  If, for some
   * reason, this is not an option, then the library code distro can be placed in the module dir in a dir
   * called 'timeline'.  In this case, it will be autodetected and loaded from the local copy.
   *
   * To Download: http://simile.mit.edu/wiki/Timeline/Download_The_Source
   */
  if (file_exists($_SERVER['DOCUMENT_ROOT'] . base_path() . drupal_get_path('module', 'timemap') ."/timeline/timeline-api.js")) {
    drupal_add_js(drupal_get_path('module', 'timemap') ."/timeline/timeline-api.js");
  }
  else {
    drupal_add_js(drupal_get_path('module', 'timemap') ."/load_timeline.js");
  }

  drupal_add_js(drupal_get_path('module', 'timemap') ."/timemap_simile-timeline.js");
  drupal_add_css(drupal_get_path('module', 'timemap') ."/timemap.css");

  return $people_chooser .'<div id="timemap" basepath="'. base_path() .'" uid="'. $uid .'" offset="'. $user->timezone .'"></div><div class="timemap_controls" id="timemap_controls"></div>';

}

function theme_timemap_viewable_reports_list_people($list = array()) {
return '';
  $ret = '<div id="timemap_report_people">View Reports for: ';
  foreach ($list as $uid => $name) {
    $ret .= '<input type="checkbox" id="timemap_person_'. $uid .'" class="timemap_report_person_selector" />';
    $ret .= '<label for="timemap_person_'. $uid .'">'. $name .'</label>';
  }
  return $ret .'</div>';
}

function timemap_map_serv($uid = null) {
  global $user;
  if (! $uid) {
    $uid = $user->uid;
  }
  $allowed = timemap_viewable_reports_list();
  if ( ! in_array($uid, array_keys($allowed)) ) {
    drupal_access_denied();
    exit();
  }

  $result = db_query("select d.cid as cid, d.task as task, d.time as time, d.stop as stop, c.category as category, cc.hex as color
                      from {timemap_doings} d
                        left join {timemap_categories} c on c.cid=d.cid
                        left join {timemap_catcolor} cc on c.cid=cc.cid
                      where d.uid = %d and d.cid >= 0
                      order by d.time", $uid);

  $return = array(
    'dateTimeFormat' => 'iso8601',
    'events' => array(),
  );
  while ($row = db_fetch_object($result)) {
    $end_k = $row->stop != '0' ? 'end' : 'earliestEnd';
    $end_v = $row->stop != '0' ? timemap_datify($row->stop) : timemap_datify(time());
    $return['events'][] = array(
      'start' => timemap_datify($row->time),
      $end_k => $end_v,
      'title' => $row->task,
      'color' => $row->color,
      'description' => 'Category: <i>'. $row->category .'</i>',
    );
  }

  print drupal_to_js($return);
  exit();
}

function timemap_datify($secs) {
  global $user;
  //$secs += $user->timezone;
  //return date(DATE_RFC3339, $secs);
  return gmdate('c', $secs);
}

function timemap_report($uid = null, $report = 'default', $from = null, $numDays = null) {
  global $user;
  if(! $uid) {
    $uid = $user->uid;
  }

  $allowed = timemap_viewable_reports_list();

  if ( ! in_array($uid, array_keys($allowed)) ) {
    drupal_access_denied();
    exit();
  }

  $return = '';

    //are we the only only person who has reports we can view, or are there more to choose from?
  if ( ! ((count($allowed) == 1) && isset($allowed[$user->uid])) ) {
    $return .= drupal_get_form('timemap_report_choose_person');
  }

  $return .= timemap_draw_report(timemap_get_report_stats($uid, $from, $numDays), $report);
  return $return;
}

function timemap_report_choose_person(&$form_state) {
  global $user;

  $form['uid'] = array(
    '#type' => 'select',
    '#options' => timemap_viewable_reports_list(),
    '#required' => true,
    '#title' => t('Person to view'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
  );

  return $form;
}
function timemap_report_choose_person_validate($form, &$form_state) {
  return true;
}
function timemap_report_choose_person_submit($form, &$form_state) {
  return 'timemap/report/'. $values['uid'];
}

function timemap_viewable_reports_list() {
  global $user;

  if ($user->uid == 1) {
    $names = array(1, $user->name);
  }
   // view all?
  if ( user_access('view all reports') || ($user->uid == 1) ) {
    $result = db_query("select distinct uid from {timemap_doings}");
    while ($row = db_fetch_object($result)) {
      $userrec = user_load(array('uid' => $row->uid));
      $names[$userrec->uid] = $userrec->name;
    }
     // just return everyone.  there's no need to do any further sorting at this point.
    return $names;
  }

  $return = array();

     // view reports defined by CiviCRM groups?
  if (user_access('view subordinate reports')) {
    $rel = variable_get('timemap_civicrm_subord_rel');
    $names = crm_get_relationships($user, null, $rel, null, null, 0, 1000);

    $return = array_merge($return, $names);
  }

   // view your own?
  if (user_access('view own report')) {
    $names = array($user->uid => $user->name);
    $return = array_merge($return, $names);
  }

  return $return;
}
/////////////////////////////////////////
/**
 *  Implementation of hook_timemap_report
 */
function timemap_timemap_report($op = 'list', $delta = 0, $stats = null) {
  switch ($op) {
    case 'list':
      $return[] = array(
        'module' => 'timemap', // The module's name that is providing this report!!!
        'title' => 'default',
        'delta' => 0,
      );
      return $return;

    case 'view':
      switch($delta) {
        case 0:
          return timemap_get_report_0($stats);
      }
      break;
  }

  return array();
}

function timemap_get_report_0($stats = null) {
  return '<pre><code>' . print_r($stats,1) . '</code></pre>';
}

function timemap_draw_report($stats, $report = 'default') {
  $avail_reports = module_invoke_all('timemap_report', 'list');
  foreach($avail_reports as $rpt) {
    if ($rpt['title'] == $report) {
      $report_detail = module_invoke($rpt['module'], 'timemap_report', 'view', $rpt['delta'], $stats);
    }
  }

   // if for some reason we've requested a report that isn't available, kick in our 'default' and notify
  if (! isset($report_detail)) {
    drupal_set_message("We cannot find a report called <i>$report</i>.  Hopefully the following information will be helpful.", 'error');
    $report_detail = module_invoke('timemap', 'timemap_report', 'view', 0, $stats);
  }

  return $report_detail;
}

/**
 *  Called from timemap_report().  Collect the stats for a given uid and date and return them.
 *  These stats are then passed to whatever report is being used to draw some pretty pictures.
 */
function timemap_get_report_stats($uid = null, $from = null, $days_to_show = null) {
  global $user;
  if (! $uid) {
    $uid = $user->uid;
  }

   // expect and set variables as days
   // we'll default to showing the "default" num of days starting from that many days ago... ie, up to today.
  if (! isset($from)) {
    $from = variable_get('timemap_report_default_days_to_show', 7);
  }
  if (! isset($days_to_show)) {
    $days_to_show = variable_get('timemap_report_default_days_to_show', 7);
  }

    // turn days into unix times relative to now.
    // 86400 = 24 * 60 * 60 --> no. secs to "see"
    // days-1 because partial today will count as a whole 1 for our purposes
  $secs = ($from - 1) * 86400;

  $lastmidnight = strtotime("12 am UTC");
  $from = $lastmidnight - $secs;
  $secs_to_show = $from + ($days_to_show * 86400);

  $result = db_query("select d.cid as cid, d.task as task, d.time as time, d.stop as stop, c.category as category
                      from {timemap_doings} d left join {timemap_categories} c on c.cid=d.cid
                      where d.uid = %d and d.time between %d and %d
                      order by d.time",
                   $uid, $from, $secs_to_show);
  $stats_obj['info'] = array(
    'days' => $days_to_show,
  );
  while ($row = db_fetch_object($result)) {
    $stats_obj['items'][] = array(
      'cid' => $row->cid,
      'category' => $row->category,
      'task' => $row->task,
      'time' => $row->time,
      'stop' => $row->stop,
    );
  }

  return $stats_obj;
}