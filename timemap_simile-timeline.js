if (Drupal.jsEnabled) { 
  var tl;
  var eventSources = new Array();
  var now = new Date();
  var uid, timezoneOffset, basepath;

  function timemap_onLoad() {
    var eventSource = new Timeline.DefaultEventSource(0);
    var theme = Timeline.ClassicTheme.create();

    var bandInfos = [
      Timeline.createBandInfo({
          eventSource:    eventSource,
          date:           now,
          width:          "65%", 
          intervalUnit:   Timeline.DateTime.HOUR,
          timeZone:       timezoneOffset, 
          intervalPixels: 100
      }),
      Timeline.createBandInfo({
          eventSource:    eventSource,
          date:           now, 
          width:          "20%", 
          intervalUnit:   Timeline.DateTime.DAY,
          timeZone:       timezoneOffset, 
          intervalPixels: 200,
          showEventText:  false,
          trackHeight:    0.6,
          trackGap:       0.2
      }),
      Timeline.createBandInfo({
          eventSource:    eventSource,
          date:           now,
          width:          "15%", 
          intervalUnit:   Timeline.DateTime.WEEK,
          timeZone:       timezoneOffset, 
          intervalPixels: 200,
          showEventText:  false,
          trackHeight:    0.3,
          trackGap:       0.2
      })
    ];
    bandInfos[1].syncWith = 0;
    bandInfos[1].highlight = true;
    bandInfos[2].syncWith = 1;
    bandInfos[2].highlight = true;
    
    Timeline.loadJSON(basepath + "index.php?q=timemap/map/serv/" + uid, function(json, url) {
      eventSource.loadJSON(json, url);
    });
    tl = Timeline.create(document.getElementById("timemap"), bandInfos);

    eventSources[0] = eventSource;    
  }

  var resizeTimerID = null;
  function timemap_onResize() {
      if (resizeTimerID == null) {
          resizeTimerID = window.setTimeout(function() {
              resizeTimerID = null;
              tl.layout();
          }, 500);
      }
  }

  jQuery().ready(function(){
    uid = $('#timemap').attr('uid');
    timezoneOffset = $('#timemap').attr('offset') / 3600;
    basepath = $('#time_map').attr('basepath');
    timemap_onLoad();
    $('body').resize('timemap_onResize');
    
    /*
    $('.time_report_person_selector').change(function() {
      var uid = this.id.replace(/time_person_/, '');
      if (this.checked) {
        var eventSource = new Timeline.DefaultEventSource(uid);
        tl.loadJSON("/index.php?q=timemap/map/serv/" + uid, function(json, url) {
          eventSources[0].loadJSON(json, url);
        });
        eventSources[uid] = eventSource;
      } else {
        var es = eventSources[uid];
        es.clear();
      }
    });
    */
    
  });
}
